
// function getName(callback) {
//     const PROTOTYPE = {
//         name: "Nelle",
//         personality: "Peasant",

//         getName(){
//             return this.name;
//         },

//         getPersonality(){
//             return this.personality;
//         },
//     };

//     if(PROTOTYPE)
//         callback(PROTOTYPE);
//     else
//         callback(false);
// }

// getName((CB_PROTOTYPE) => { //1
//     for (let i = 1; i <= 10; i++) {
//         console.log(`${CB_PROTOTYPE.getName()}    ${CB_PROTOTYPE.getPersonality()}    ${i}`);
//     }
// })

// ----------------------------------------------------------------------------------------------------
// CALLBACK HELL
// ----------------------------------------------------------------------------------------------------

// function callbackHell(cb_a) {
//     console.log("INIT CALLBACK HELL");
//     cb_a((cb_c) => {
//         console.log("CB B");
//         cb_c((cb_e) => {
//             console.log("CB D");
//             cb_e();
//         });
//     })
// }

// callbackHell((cb_b) => {
//     console.log("CB A");
//     cb_b((cb_d) => {
//         console.log("CB C");
//         cb_d(() => {
//             console.log("CB E");
//         });
//     });
// })



// ----------------------------------------------------------------------------------------------------
// TESTING HTTP / URL / FS modules
// ----------------------------------------------------------------------------------------------------


const http = require('http');
const url = require('url');
const myMod = require('./firstmodule');
const fs = require('fs');


// http.createServer(function (req, res) {
//     let q = url.parse(req.url, true);

//     fs.readFile(`${q.pathname.substring(1)}.html`, (err, data) => {
//         if(data === "" || data === null || data === undefined) {
//             res.writeHead(301, {"Location": 'http://localhost:222/index'});
//             return res.end();
//         } else {
//             res.writeHead(200, {'Content-Type': 'text/html'});
//             res.end(data);
//         }
//     })

//     // fs.readFile(`data/data.json`, (err, data) => {
//     //     res.writeHead(200, {'Content-Type': 'application/json'});
//     //     res.end(JSON.stringify(data));
//     // })

// }).listen(222);

// ----------------------------------------------------------------------------------------------------
// TESTING EXPRESS
// ----------------------------------------------------------------------------------------------------

const express = require('express');
const app = express();
const port = 222;

const getVerification = (req, res, next) => {
    console.log("I'm inside the Middleware");
    if(req.method === "GET") 
        next();
    else
        res.send('YOU NEED TO SET TO GET!');
}  
                  
app.use(getVerification);

app.get('/', (req, res) => {
    res.send('Hello World!');
})

app.get('/data', (req, res) => {
    fs.readFile(`data/data.json`, (err, data) => {
        res.setHeader('Content-Type', 'application/json');
        res.send(data);
    })
})

app.listen(port, () => {
  console.log(`Listening on Port [ ${port} ]`);
})